//Import thu vien ExpressJS
//import express form 'express'
const express = require('express');
//Import thu vien mongoose
const mongoose = require('mongoose');
//Khai bao model mongoose
const reviewModel = require('./app/models/reviewModel');
const courseModel = require('./app/models/courseModel');

const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");

//Khoi tao app node.js bang express
const app = express();
// Kết nối với MongoDB
// Kết nối với MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course")
  .then(() => {
    console.log('Successfully connected');
  })
  .catch((error) => {
    throw error;
  });
// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());



//Khai bao ung dung se chay cong 8000
const port = 8000;
//middleware log ra thoi gian hien tai   
app.use((req, res, next) => {
  console.log(new Date());
  next();
}, (req, res, next) => {
  console.log(req.method);
  next();
});

//Lay ra ngay thang hom nay
app.get("/", (req, res) => {
  const today = new Date();
  res.status(200).json({
    message: `Hom nay la ngay ${today.getDate()} thang ${today.getMonth() + 1} nam ${today.getFullYear()}`
  })
});

app.use("/api/v1/courses", courseRouter);
app.use("/api/v1/reviews", reviewRouter);

//Chay ung dung tren cong port:8000
app.listen(port, () => {
  console.log("Ung dung chay tren cong : ", port)
});