const getAllCourseMiddleware = (req, res, next) => {
    console.log("Get All Course!");
    next();
}

const getACourseMiddleware = (req, res, next) => {
    console.log("Get a Course!");
    next();
}
const postCourseMiddleware = (req, res, next) => {
    console.log("Create a Course!");
    next();
}

const putCourseMiddleware = (req, res, next) => {
    console.log("Update a Course!");
    next();
}
const deleteCourseMiddleware = (req, res, next) => {
    console.log("Delete a Course!");
    next();
}

//export
module.exports = {
    getAllCourseMiddleware,
    getACourseMiddleware,
    postCourseMiddleware,
    putCourseMiddleware,
    deleteCourseMiddleware
}