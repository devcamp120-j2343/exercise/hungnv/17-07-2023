//B1: Khai bao thu vien mongoose
const mongoose = require('mongoose');

//B2: Khai bao thu vien Schema cua mongoose
const Schema = mongoose.Schema;

//B3: Tao doi tuong Schema bao gom cac thuoc tinh cua collection
const courseSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    title: {
        type: String,
        require: true,
        unique: true
    },
    description: {
        type: String,
        require: false
    },
    student: {
        type: Number,
        default: 0
    },
    reviews: [{
        type: mongoose.ObjectId,
        ref: "review"
    }]
});

//B4: export Schema ra model
module.exports = mongoose.model("course", courseSchema);