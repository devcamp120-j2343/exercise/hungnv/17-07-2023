//B1: Khai bao thu vien mongoose
const mongoose = require('mongoose');

//B2: Khai bao thu vien Schema cua mongoose
const Schema = mongoose.Schema;

//B3: Tao doi tuong Schema bao gom cac thuoc tinh cua collection
const reviewSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    // _id:{
    //     type:mongoose.Types.ObjectId
    // }
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    },
    create_At: {
        type: Date,
        default: Date.now()
    },
    update_At: {
        type: Date,
        default: Date.now()
    },
});

//B4: export Schema ra model
module.exports = mongoose.model("review", reviewSchema);