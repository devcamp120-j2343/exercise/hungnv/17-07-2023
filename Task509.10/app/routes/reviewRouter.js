//Import thu vien express
const express = require("express");
//Khai bao middleware
const reviewMiddleware = require("../middleware/reviewMiddleware");

//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.get("/", reviewMiddleware.getAllReviewMiddleware, (req, res) => {
    res.json({
        message: "Get all courses"
    })
})

router.post("/", reviewMiddleware.postReviewMiddleware, (req, res) => {
    res.json({
        message: "Create course"
    })
});

router.get("/:courseId", reviewMiddleware.getAReviewMiddleware, (req, res) => {
    var courseId = req.params.courseId;

    res.json({
        message: "Get course id = " + courseId
    })
});

router.put("/:courseId", reviewMiddleware.putReviewMiddleware, (req, res) => {
    var courseId = req.params.courseId;

    res.json({
        message: "Update course id = " + courseId
    })
});

router.delete("/:courseId", reviewMiddleware.deleteReviewMiddleware, (req, res) => {
    var courseId = req.params.courseId;

    res.json({
        message: "Delete course id = " + courseId
    })
});

module.exports = router;
