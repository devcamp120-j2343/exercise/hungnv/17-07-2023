//Import thu vien express
const express = require("express");
//Khai bao middleware
const courseMiddleware = require("../middleware/courseMiddleware");
//import controller
const { getAllCourse, getCourseById, createCourse, updateCourse, deleteCourse } = require("../controller/courseController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
//get all course
router.get("/courses", courseMiddleware.getAllCourseMiddleware, getAllCourse)
//create course
router.post("/courses", courseMiddleware.postCourseMiddleware, createCourse);
//get a course
router.get("/courses/:courseId", courseMiddleware.getACourseMiddleware, getCourseById);
//update course
router.put("/courses/:courseId", courseMiddleware.putCourseMiddleware, updateCourse);
//delete course
router.delete("/courses/:courseId", courseMiddleware.deleteCourseMiddleware, deleteCourse);

module.exports = router;


