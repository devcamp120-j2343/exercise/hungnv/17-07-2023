//Import course model
const { default: mongoose } = require('mongoose');
const courseModel = require('../models/courseModel');

//Tao cac function crud
//get all course
const getAllCourse = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    courseModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all courses sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

//get a course
const getCourseById = (req, res) => {
    //B1: thu thập dữ liệu
    var courseId = req.params.courseId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    courseModel.findById(courseId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get course by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a course
const createCourse = (req, res) => {
    //B1: thu thập dữ liệu
    const { title, description, student } = req.body;

    //B2: kiểm tra dữ liệu
    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required!"
        })
    }

    if (!Number.isInteger(student) || student < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "student is invalid!"
        })
    }

    //B3: thực hiện thao tác model
    let newCourse = {
        _id: new mongoose.Types.ObjectId(),
        title,
        description,
        student
    }

    courseModel.create(newCourse)
        .then((data) => {
            return res.status(201).json({
                status: "Create new course sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

//update course
const updateCourse = (req, res) => {
    //B1: thu thập dữ liệu
    var courseId = req.params.courseId;
    const { title, description, student } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required!"
        })
    }

    if (!Number.isInteger(student) || student < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "student is invalid!"
        })
    }

    //B3: thực thi model
    let updateCourse = {
        title,
        description,
        student
    }

    courseModel.findByIdAndUpdate(courseId, updateCourse)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update course sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

//delete course
const deleteCourse = (req, res) => {
    //B1: Thu thập dữ liệu
    var courseId = req.params.courseId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    courseModel.findByIdAndDelete(courseId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete course sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

//Export thanh module
module.exports = {
    getAllCourse,
    getCourseById,
    createCourse,
    updateCourse,
    deleteCourse
}