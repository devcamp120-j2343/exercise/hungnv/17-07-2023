//B1: Khai bao thu vien mongoose
const mongoose = require('mongoose');

//B2: Khai bao thu vien Schema cua mongoose
const Schema = mongoose.Schema;

//B3: Tao doi tuong Schema bao gom cac thuoc tinh cua collection
const orderSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    orderCode:{
        type:String,
        unique:true
    },
    pizzaSize:{
        type:String,
        required:true
    },
    pizzaType:{
        type:String,
        required:true
    },
    voucher:{
        type:mongoose.Types.ObjectId,
        ref:'voucher'
    },
    drink:{
        type:mongoose.Types.ObjectId,
        ref:'drink'
    },
    status:{
        type:String,
        required:true
    }
});
//B4: export Schema ra model
module.exports = mongoose.model("order", orderSchema);