//B1: Khai bao thu vien mongoose
const mongoose = require('mongoose');

//B2: Khai bao thu vien Schema cua mongoose
const Schema = mongoose.Schema;

//B3: Tao doi tuong Schema bao gom cac thuoc tinh cua collection
const userSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    fullName:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    address:{
        type:String,
        required:true,
    },
    phone:{
        type:String,
        required:true,
        unique:true
    },
    orders:[{
        type: mongoose.ObjectId,
        ref:'order'
    }]
});
//B4: export Schema ra model
module.exports = mongoose.model("user", userSchema);