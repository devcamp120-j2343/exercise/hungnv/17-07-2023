//B1: Khai bao thu vien mongoose
const mongoose = require('mongoose');

//B2: Khai bao thu vien Schema cua mongoose
const Schema = mongoose.Schema;

//B3: Tao doi tuong Schema bao gom cac thuoc tinh cua collection
const voucherSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    maVoucher:{
        type:String,
        unique:true,
        required:true
    },
    phanTramGiamGia:{
        type:Number,
        required:true
    },
    ghiChu:{
        type:String,
        required:false
    }
});
//B4: export Schema ra model
module.exports = mongoose.model("voucher", voucherSchema);