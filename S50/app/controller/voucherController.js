//Import course model
const { default: mongoose } = require('mongoose');
const voucherModel = require('../models/voucherModel');

//Tao cac function crud
//get all course
const getAllVoucher = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    voucherModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all voucher sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any voucher",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
const getVcouherById = (req, res) => {
    //B1: thu thập dữ liệu
    var voucherId = req.params.voucherId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    voucherModel.findById(voucherId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get voucher by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any voucher",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a drink
const createVoucher = (req, res) => {
    //B1: thu thập dữ liệu
    const { maVoucher, phanTramGiamGia, ghiChu } = req.body;

    //B2: kiểm tra dữ liệu
    if (!maVoucher) {
        return res.status(400).json({
            status: "Bad request",
            message: "ma voucher is required!"
        })
    }
    if (!ghiChu) {
        return res.status(400).json({
            status: "Bad request",
            message: "ghi chu is required!"
        })
    }
    if (!Number.isInteger(phanTramGiamGia) || phanTramGiamGia < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "phanTramGiamGia is invalid!"
        })
    }

    //B3: thực hiện thao tác model
    let newVoucher = {
        _id: new mongoose.Types.ObjectId(),
        maVoucher,
        phanTramGiamGia,
        ghiChu
    }

    voucherModel.create(newVoucher)
        .then((data) => {
            return res.status(201).json({
                status: "Create new voucher sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//update drink
const updateVoucher = (req, res) => {
    //B1: thu thập dữ liệu
    var voucherId = req.params.voucherId;
    const { maVoucher, phanTramGiamGia, ghiChu } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!maVoucher) {
        return res.status(400).json({
            status: "Bad request",
            message: "ma voucher is required!"
        })
    }
    if (!ghiChu) {
        return res.status(400).json({
            status: "Bad request",
            message: "ghi chu is required!"
        })
    }
    if (!Number.isInteger(phanTramGiamGia) || phanTramGiamGia < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "phanTramGiamGia is invalid!"
        })
    }

    //B3: thực thi model
    let updateVoucher = {
        maVoucher,
        phanTramGiamGia,
        ghiChu
    }

    voucherModel.findByIdAndUpdate(voucherId, updateVoucher)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update voucher sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any voucher",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}
//delete drink
const deleteVoucher = (req, res) => {
    //B1: Thu thập dữ liệu
    var voucherId = req.params.voucherId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    voucherModel.findByIdAndDelete(voucherId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete voucher sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any voucher",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

//Export thanh module
module.exports = {
    getAllVoucher,
    getVcouherById,
    createVoucher,
    updateVoucher,
    deleteVoucher
}