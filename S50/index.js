const express = require("express"); // Tương tự : import express from "express";
const path = require("path");
//Import thu vien mongoose
const mongoose = require('mongoose');
//Khai bao model mongoose
const drinkModel = require('./app/models/drinkModel');
const voucherModel = require('./app/models/voucherModel');
const orderModel = require('./app/models/orderModel');
const userModel = require('./app/models/userModel');
//Khai bao router
const drinkRouter = require("./app/router/drinkRouter");
const voucherRouter = require("./app/router/voucherRouter");
// Khởi tạo Express App
const app = express();
// Kết nối với MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
  .then(() => {
    console.log('Successfully connected to MongooseDB');
  })
  .catch((error) => {
    throw error;
  });

// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());

//Khai bao ung dung se chay cong 8000
const port = 8000;

//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/views"));

// Khai báo APi dạng Get "/" sẽ chạy vào đây
// app.get("/", (request, response) => {
//     console.log(__dirname);

//     response.sendFile(path.join(__dirname + "/views/pizza365index.html"));
// })

// Đường dẫn gốc trả về file giao diện trang chủ course365
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "views", "pizza365index.html"));
});
app.use("/api", drinkRouter);
app.use("/api", voucherRouter);

app.listen(port, () => {
  console.log(`App Listening on port ${port}`);
})
